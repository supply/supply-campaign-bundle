<?php
/**
 * Created by PhpStorm.
 * User: hpenny
 * Date: 4/08/16
 * Time: 3:50 PM
 */

namespace Supply\CampaignBundle\Helper\Prize;


use Supply\CampaignBundle\Entity\Prize;

class AvailablePrize
{
    /** @var Prize $prize */
    protected $prize;

    /** @var callable $reserver */
    protected $reserver;

    public function __construct(Prize $prize = null, $reserver = null, $sessionId = null)
    {
        $this->prize = $prize;
        $this->reserver = $reserver;
        $this->sessionId = $sessionId;
    }

    public function getPrize()
    {
        return $this->prize;
    }

    public function reserve(\DateTime $until)
    {
        if(!$this->prize || !$this->reserver || !$this->sessionId) {
            throw new \Exception('Prize not available to reserve');
        }
        $this->prize = call_user_func($this->reserver, $until, $this->sessionId);
    }

    public function isAvailable($sessionId = null)
    {
        if(!$this->prize) {
            //echo "false: no prize\n";
            return false;
        }

        if(!$this->prize->getPending()) {
            //echo "true: Not pending\n";
            return true;
        }

        $_sessionId = $sessionId?$sessionId:$this->sessionId;

        if($this->prize->getPendingSessionId() == $_sessionId) {
            //echo "true: session matches " . $_sessionId . "<br>\n";
            return true;
        }

        if($this->prize->getPending() < new \DateTime()) {
            //echo "true: pending date expired";
            return true;
        }

        if($this->prize->getPendingSessionId() != $_sessionId) {
            //echo "false: session mis-match\n";
            return false;
        }

        //echo "false\n";
        return false;
    }
}
