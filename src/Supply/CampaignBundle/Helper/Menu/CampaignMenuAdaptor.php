<?php

namespace Supply\CampaignBundle\Helper\Menu;

use Kunstmaan\AdminBundle\Helper\Menu\TopMenuItem;
use Kunstmaan\AdminBundle\Helper\Menu\MenuItem;
use Kunstmaan\AdminBundle\Helper\Menu\MenuBuilder;
use Kunstmaan\AdminBundle\Helper\Menu\MenuAdaptorInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class CampaignMenuAdaptor implements MenuAdaptorInterface
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * CampaignMenuAdaptor constructor.
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, TokenStorageInterface $tokenStorage)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * In this method you can add children for a specific parent, but also remove and change the already created children
     *
     * @param MenuBuilder $menu The MenuBuilder
     * @param MenuItem[]  &$children The current children
     * @param MenuItem $parent  The parent Menu item
     * @param Request  $request The Request
     */
    public function adaptChildren(MenuBuilder $menu, array &$children, MenuItem $parent = null, Request $request = null)
    {
        if (is_null($parent)) {

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_index');
            $menuitem->setLabel('Campaign');
            $menuitem->setParent($parent);
            $menuitem->setWeight(5);
            if (
                stripos($request->attributes->get('_route'), 'supplycampaignbundle_admin') !== false
            ) {
                $menuitem->setActive(true);
            }
            $children[] = $menuitem;
        }

        if (!is_null($parent) && 'supplycampaignbundle_admin_index' == $parent->getRoute() && $parent->getLabel() != 'Campaign dashboard') {

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_campaign');
            $menuitem->setLabel('Edit campaigns');
            $menuitem->setParent($parent);
            $menuitem->setWeight(-40);
            if (stripos($request->attributes->get('_route'), 'supplycampaignbundle_admin_campaign') !== false) {
                $menuitem->setActive(true);
            }
            $children[] = $menuitem;
//
//
//            $menuitem = new TopMenuItem($menu);
//            $menuitem->setRoute('supplycampaignbundle_admin_winner');
//            $menuitem->setLabel('Edit Winners');
//            $menuitem->setParent($parent);
//            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
//                $menuitem->setActive(true);
//                $parent->setActive(true);
//            }
//            $children[] = $menuitem;
//
//            $menuitem = new TopMenuItem($menu);
//            $menuitem->setRoute('supplycampaignbundle_admin_winner_add');
//            $menuitem->setLabel('Add Winner');
//            $menuitem->setParent($parent);
//            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
//                $menuitem->setActive(true);
//                $parent->setActive(true);
//            }
//            $children[] = $menuitem;

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_prize');
            $menuitem->setLabel('Edit Prizes');
            $menuitem->setParent($parent);
            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
                $menuitem->setActive(true);
                $parent->setActive(true);
            }
            $children[] = $menuitem;

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_prize_add');
            $menuitem->setLabel('Add Prize');
            $menuitem->setParent($parent);
            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
                $menuitem->setActive(true);
                $parent->setActive(true);
            }
            $children[] = $menuitem;

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_instantprize');
            $menuitem->setLabel('Edit Instant Prizes');
            $menuitem->setParent($parent);
            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
                $menuitem->setActive(true);
                $parent->setActive(true);
            }
            $children[] = $menuitem;

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_instantprize_add');
            $menuitem->setLabel('Add Instant Prize');
            $menuitem->setParent($parent);
            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
                $menuitem->setActive(true);
                $parent->setActive(true);
            }
            $children[] = $menuitem;

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_entry');
            $menuitem->setLabel('Edit Entries');
            $menuitem->setParent($parent);
            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
                $menuitem->setActive(true);
                $parent->setActive(true);
            }
            $children[] = $menuitem;

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_entry_add');
            $menuitem->setLabel('Add Entry');
            $menuitem->setParent($parent);
            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
                $menuitem->setActive(true);
                $parent->setActive(true);
            }
            $children[] = $menuitem;

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_code');
            $menuitem->setLabel('Edit Codes');
            $menuitem->setParent($parent);
            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
                $menuitem->setActive(true);
                $parent->setActive(true);
            }
            $children[] = $menuitem;

            $menuitem = new TopMenuItem($menu);
            $menuitem->setRoute('supplycampaignbundle_admin_code_add');
            $menuitem->setLabel('Add Code');
            $menuitem->setParent($parent);
            if ($request->attributes->get('_route') === $menuitem->getRoute()) {
                $menuitem->setActive(true);
                $parent->setActive(true);
            }
            $children[] = $menuitem;
        }
    }
}
