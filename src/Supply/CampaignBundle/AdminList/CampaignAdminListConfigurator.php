<?php

namespace Supply\CampaignBundle\AdminList;

use Doctrine\ORM\EntityManager;

use Supply\CampaignBundle\Form\CampaignAdminType;
use Kunstmaan\AdminListBundle\AdminList\FilterType\ORM;
use Kunstmaan\AdminListBundle\AdminList\Configurator\AbstractDoctrineORMAdminListConfigurator;
use Kunstmaan\AdminBundle\Helper\Security\Acl\AclHelper;
use Kunstmaan\AdminListBundle\AdminList\SortableInterface;

/**
 * The admin list configurator for Campaign
 */
class CampaignAdminListConfigurator extends AbstractDoctrineORMAdminListConfigurator {
    /**
     * @param EntityManager $em        The entity manager
     * @param AclHelper     $aclHelper The acl helper
     */
    public function __construct(EntityManager $em, AclHelper $aclHelper = null)
    {
        parent::__construct($em, $aclHelper);
        $this->setAdminType(new CampaignAdminType());
    }

    /**
     * Configure the visible columns
     */
    public function buildFields()
    {
        $this->addField('name', 'Name', true);
        $this->addField('gaCode', 'Ga code', true);
        $this->addField('submitTracking', 'Submit tracking', true);
    }

    /**
     * Build filters for admin list
     */
    public function buildFilters()
    {
        $this->addFilter('name', new ORM\StringFilterType('name'), 'Name');
        $this->addFilter('gaCode', new ORM\StringFilterType('gaCode'), 'Ga code');
        $this->addFilter('submitTracking', new ORM\StringFilterType('submitTracking'), 'Submit tracking');
    }

    /**
     * Get bundle name
     *
     * @return string
     */
    public function getBundleName()
    {
        return 'SupplyCampaignBundle';
    }

    /**
     * Get entity name
     *
     * @return string
     */
    public function getEntityName()
    {
        return 'Campaign';
    }

    public function canExport()
    {
        return true;
    }
}
