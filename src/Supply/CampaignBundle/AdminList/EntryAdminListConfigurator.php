<?php

namespace Supply\CampaignBundle\AdminList;

use Doctrine\ORM\EntityManager;

use Supply\CampaignBundle\Form\EntryAdminType;
use Kunstmaan\AdminListBundle\AdminList\FilterType\ORM;
use Kunstmaan\AdminListBundle\AdminList\Configurator\AbstractDoctrineORMAdminListConfigurator;
use Kunstmaan\AdminBundle\Helper\Security\Acl\AclHelper;
use Kunstmaan\AdminListBundle\AdminList\SortableInterface;

/**
 * The admin list configurator for Entry
 */
class EntryAdminListConfigurator extends AbstractDoctrineORMAdminListConfigurator {
    /**
     * @param EntityManager $em        The entity manager
     * @param AclHelper     $aclHelper The acl helper
     */
    public function __construct(EntityManager $em, AclHelper $aclHelper = null)
    {
        parent::__construct($em, $aclHelper);
        $this->setAdminType(new EntryAdminType());
    }

    /**
     * Configure the visible columns
     */
    public function buildFields()
    {
        $this->addField('firstName', 'First Name', true);
        $this->addField('lastName', 'Last Name', true);
        $this->addField('emailAddress', 'Email address', true);
        $this->addField('phoneNumber', 'Phone number', true);
        $this->addField('entryDate', 'Entry date', true);
        $this->addField('channel', 'Channel', true);
        $this->addField('prize', 'Prize', false, 'SupplyCampaignBundle:AdminList\Entry:prize.html.twig');
    }

    /**
     * Build filters for admin list
     */
    public function buildFilters()
    {
        $this->addFilter('firstName', new ORM\StringFilterType('firstName'), 'First name');
        $this->addFilter('lastName', new ORM\StringFilterType('lastName'), 'Last name');
        $this->addFilter('emailAddress', new ORM\StringFilterType('emailAddress'), 'Email address');
        $this->addFilter('phoneNumber', new ORM\StringFilterType('phoneNumber'), 'Phone number');
        $this->addFilter('entryDate', new ORM\DateFilterType('entryDate'), 'Entry date');
        $this->addFilter('channel', new ORM\StringFilterType('channel'), 'Channel');
    }


    /**
     * Configure the export fields
     */
    public function buildExportFields()
    {
        $this->addExportField('firstName', 'First Name');
        $this->addExportField('lastName', 'Last Name');
        $this->addExportField('emailAddress', 'Email address');
        $this->addExportField('phoneNumber', 'Phone number');

        $this->addExportField('address', 'Address');
        $this->addExportField('suburb', 'Suburb');
        $this->addExportField('postcode', 'Postcode');
        $this->addExportField('city', 'City');

        $this->addExportField('entryDate', 'Entry date');
        $this->addExportField('channel', 'Channel');
        $this->addExportField('prize', 'Prize', 'SupplyCampaignBundle:AdminList\Entry:prize.html.twig');
    }

    /**
     * Get bundle name
     *
     * @return string
     */
    public function getBundleName()
    {
        return 'SupplyCampaignBundle';
    }

    /**
     * Get entity name
     *
     * @return string
     */
    public function getEntityName()
    {
        return 'Entry';
    }

    public function canExport()
    {
        return true;
    }
}
