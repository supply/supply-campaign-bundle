<?php

namespace Supply\CampaignBundle\AdminList;

use Doctrine\ORM\EntityManager;

use Supply\CampaignBundle\Form\PrizeAdminType;
use Kunstmaan\AdminListBundle\AdminList\FilterType\ORM;
use Kunstmaan\AdminListBundle\AdminList\Configurator\AbstractDoctrineORMAdminListConfigurator;
use Kunstmaan\AdminBundle\Helper\Security\Acl\AclHelper;
use Kunstmaan\AdminListBundle\AdminList\SortableInterface;

/**
 * The admin list configurator for Prize
 */
class PrizeAdminListConfigurator extends AbstractDoctrineORMAdminListConfigurator {
    /**
     * @param EntityManager $em        The entity manager
     * @param AclHelper     $aclHelper The acl helper
     */
    public function __construct(EntityManager $em, AclHelper $aclHelper = null)
    {
        parent::__construct($em, $aclHelper);
        $this->setAdminType(new PrizeAdminType());
    }

    /**
     * Configure the visible columns
     */
    public function buildFields()
    {
        $this->addField('title', 'Title', true);
        $this->addField('activation', 'Activation', true);
        $this->addField('pending', 'Pending', true);
        $this->addField('pendingSessionId', 'Pending Session Id', true);
        $this->addField('type', 'Type', true);
        $this->addField('won', 'Won', true);
    }

    /**
     * Build filters for admin list
     */
    public function buildFilters()
    {
        $this->addFilter('title', new ORM\StringFilterType('title'), 'Title');
        $this->addFilter('activation', new ORM\DateFilterType('activation'), 'Activation');
        $this->addFilter('pending', new ORM\DateFilterType('pending'), 'Pending');
        $this->addFilter('pendingSessionId', new ORM\StringFilterType('pendingSessionId'), 'Pending Session Id');
        $this->addFilter('type', new ORM\StringFilterType('type'), 'Type');
        $this->addFilter('won', new ORM\BooleanFilterType('won'), 'Won');
    }

    /**
     * Get bundle name
     *
     * @return string
     */
    public function getBundleName()
    {
        return 'SupplyCampaignBundle';
    }

    /**
     * Get entity name
     *
     * @return string
     */
    public function getEntityName()
    {
        return 'Prize';
    }

    public function canExport()
    {
        return true;
    }
}
