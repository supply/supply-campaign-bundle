<?php

namespace Supply\CampaignBundle\AdminList;

use Doctrine\ORM\EntityManager;

use Supply\CampaignBundle\Form\InstantPrizeAdminType;
use Kunstmaan\AdminListBundle\AdminList\FilterType\ORM;
use Kunstmaan\AdminListBundle\AdminList\Configurator\AbstractDoctrineORMAdminListConfigurator;
use Kunstmaan\AdminBundle\Helper\Security\Acl\AclHelper;
use Kunstmaan\AdminListBundle\AdminList\SortableInterface;

/**
 * The admin list configurator for InstantPrize
 */
class InstantPrizeAdminListConfigurator extends AbstractDoctrineORMAdminListConfigurator {
    /**
     * @param EntityManager $em        The entity manager
     * @param AclHelper     $aclHelper The acl helper
     */
    public function __construct(EntityManager $em, AclHelper $aclHelper = null)
    {
        parent::__construct($em, $aclHelper);
        $this->setAdminType(new InstantPrizeAdminType());
    }

    /**
     * Configure the visible columns
     */
    public function buildFields()
    {
        $this->addField('title', 'Title', true);
        $this->addField('subject', 'Subject', true);
        $this->addField('copy', 'Copy', true);
        $this->addField('smsSubject', 'Sms subject', true);
        $this->addField('smsCopy', 'Sms copy', true);
        $this->addField('weight', 'Weight', true);
        $this->addField('tsAndCs', 'Ts and cs', true);
    }

    /**
     * Build filters for admin list
     */
    public function buildFilters()
    {
        $this->addFilter('title', new ORM\StringFilterType('title'), 'Title');
        $this->addFilter('subject', new ORM\StringFilterType('subject'), 'Subject');
        $this->addFilter('copy', new ORM\StringFilterType('copy'), 'Copy');
        $this->addFilter('smsSubject', new ORM\StringFilterType('smsSubject'), 'Sms subject');
        $this->addFilter('smsCopy', new ORM\StringFilterType('smsCopy'), 'Sms copy');
        $this->addFilter('weight', new ORM\NumberFilterType('weight'), 'Weight');
        $this->addFilter('tsAndCs', new ORM\StringFilterType('tsAndCs'), 'Ts and cs');
    }

    /**
     * Get bundle name
     *
     * @return string
     */
    public function getBundleName()
    {
        return 'SupplyCampaignBundle';
    }

    /**
     * Get entity name
     *
     * @return string
     */
    public function getEntityName()
    {
        return 'InstantPrize';
    }

    public function canExport()
    {
        return true;
    }
}
