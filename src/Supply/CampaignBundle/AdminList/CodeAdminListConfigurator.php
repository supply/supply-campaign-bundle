<?php

namespace Supply\CampaignBundle\AdminList;

use Hmp\KumaExtraBundle\AdminList\FilterType\ORM\EntityFilterType;
use Doctrine\ORM\EntityManager;

use Supply\CampaignBundle\Form\CodeAdminType;
use Kunstmaan\AdminListBundle\AdminList\FilterType\ORM;
use Kunstmaan\AdminListBundle\AdminList\Configurator\AbstractDoctrineORMAdminListConfigurator;
use Kunstmaan\AdminBundle\Helper\Security\Acl\AclHelper;

/**
 * The admin list configurator for Code
 */
class CodeAdminListConfigurator extends AbstractDoctrineORMAdminListConfigurator
{
    /**
     * @param EntityManager $em        The entity manager
     * @param AclHelper     $aclHelper The acl helper
     */
    public function __construct(EntityManager $em, AclHelper $aclHelper = null)
    {
        parent::__construct($em, $aclHelper);
        $this->setAdminType(new CodeAdminType());
    }

    /**
     * Configure the visible columns
     */
    public function buildFields()
    {
        $this->addField('campaign', 'Campaign', true, 'SupplyCampaignBundle:AdminList\Campaign:view.html.twig');
        $this->addField('code', 'Code', true);
        $this->addField('submitted', 'Submitted / Claimed', true);
        $this->addField('submittedAt', 'Submitted / Claimed at', true);
    }

    /**
     * Build filters for admin list
     */
    public function buildFilters()
    {
        $this->addFilter('campaign', new EntityFilterType('campaign', 'b', 'name', 'c'), 'Campaign');
        $this->addFilter('code', new ORM\StringFilterType('code'), 'Code');
        $this->addFilter('submitted', new ORM\BooleanFilterType('submitted'), 'Submitted / Claimed');
        $this->addFilter('submittedAt', new ORM\DateFilterType('submittedAt'), 'Submitted / Claimed at');
    }

    /**
     * Get bundle name
     *
     * @return string
     */
    public function getBundleName()
    {
        return 'SupplyCampaignBundle';
    }

    /**
     * Get entity name
     *
     * @return string
     */
    public function getEntityName()
    {
        return 'Code';
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return 100;
    }

    public function canExport()
    {
        return true;
    }

    /**
     * Configure the export fields
     */
    public function buildExportFields()
    {
        parent::buildExportFields();
    }

}
