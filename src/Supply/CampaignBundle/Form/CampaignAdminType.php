<?php

namespace Supply\CampaignBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

/**
 * The type for Campaign
 */
class CampaignAdminType extends AbstractType
{
    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting form the
     * top most type. Type extensions can further modify the form.
     *
     * @see FormTypeExtensionInterface::buildForm()
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('gaCode', null, [
            'attr' => [
                'style' => 'font-family: monospace; font-weight: bold;'
            ]
        ]);
        $builder->add('submitTracking', null, [
            'attr' => [
                'style' => 'font-family: monospace; font-weight: bold; width: 100%; max-width: 100%;'
            ]
        ]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'campaign_form';
    }
}
