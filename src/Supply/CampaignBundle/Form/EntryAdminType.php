<?php

namespace Supply\CampaignBundle\Form;

use Supply\Campaign\Entity\Pages\StorePage;
use Supply\Campaign\Entity\Region;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Kunstmaan\NodeBundle\Entity\NodeTranslation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

/**
 * The type for Entry
 */
class EntryAdminType extends AbstractType
{
    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting form the
     * top most type. Type extensions can further modify the form.
     *
     * @see FormTypeExtensionInterface::buildForm()
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ContainerInterface $container */
        $container = $options['container'];

        $builder
            ->add('channel', 'choice', [
                    'choices' => ['instant', 'social', 'second-chance']
                ]
            )
            ->add('firstName')
            ->add('lastName')
            ->add('emailAddress')
            ->add('phoneNumber', null, [
                'label' => 'Mobile Number',
            ])
            ->add('city')
            ->add('address')
            ->add('suburb')
            ->add('city')
            ->add('postCode')
            ->add('ageVerify')
            ->add('terms')
            ->add('optIn');
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'entry_form';
    }

    public function getParent()
    {
        return 'container_aware';
    }
}
