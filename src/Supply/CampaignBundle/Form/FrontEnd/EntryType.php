<?php

namespace Supply\CampaignBundle\Form\FrontEnd;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Kunstmaan\NodeBundle\Entity\NodeTranslation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;

/**
 * The type for Entry
 */
class EntryType extends AbstractType
{
	/** @var ContainerInterface $container */
	protected $container;

    protected $channel = 'instant';

    public function setChannel($channel)
    {
        $this->channel = $channel;
    }

    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting form the
     * top most type. Type extensions can further modify the form.
     *
     * @see FormTypeExtensionInterface::buildForm()
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var ContainerInterface $container */
        $this->container = $options['container'];

	    $builder
            ->add('receiptEntryCode', null, ['required' => true])
            ->add('firstName', null, [
            	'required' => true,
                'attr' => [ 'data-fv-notempty' => "true", "data-fv-notempty-message" => "Please enter your first name"]
            ])
            ->add('lastName', null, [
            	'required' => true,
	            'attr' => [ 'data-fv-notempty' => "true", "data-fv-notempty-message" => "Please enter your last name"]
            ])
            ->add('emailAddress', null, [
            	'required' => true,
                'attr' => ["data-fv-notempty" => "true", "data-fv-notempty-message" => "Please enter your email address", "data-fv-emailaddress" => "true", "data-fv-emailaddress-message" => "Not a valid email address"]
            ])
		    ->add('channel', 'hidden', ['required' => true])
	    ;
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::finishView($view, $form, $options);

        // Follow is to add test values to forms.
        /** @var Request $request */
        $request = $this->container->get('request');

        if($request->query->has('test')) {
            foreach($view->children as $index => $child) {
                if(!$view->children[$index]->vars['value']) {
                    if(isset($child->vars['attr']['data-fv-emailaddress'])) {
                        $view->children[$index]->vars['value'] = 'supply' . time() . '@supply.net.nz';
                    }
                    else {
                        $view->children[$index]->vars['value'] = $index . ' : ' . microtime();
                    }
                }
            }
        }

        $view->children['channel']->vars = array_merge($view->children['channel']->vars, ['value' => $this->channel]);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'entry_form_frontend';
    }

    public function getParent()
    {
        return 'container_aware';
    }
}
