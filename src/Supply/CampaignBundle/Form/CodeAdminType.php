<?php

namespace Supply\CampaignBundle\Form;

use Hmp\KumaExtraBundle\Form\FormHelper;
use Supply\CampaignBundle\Entity\Campaign;
use Supply\CampaignBundle\Entity\InstantPrize;
use Supply\CampaignBundle\Entity\Prize;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

/**
 * The type for Code
 */
class CodeAdminType extends AbstractType
{
    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting form the
     * top most type. Type extensions can further modify the form.
     *
     * @see FormTypeExtensionInterface::buildForm()
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $fb = new FormHelper($builder, $options);
        $fb
            ->add('code')
            ->addEntityDropdown('campaign', 'Supply\\CampaignBundle\\Entity\\Campaign', function(Campaign $campaign) {
                return $campaign->getName();
            })
            ->addEntityDropdown('instantPrize', 'Supply\\CampaignBundle\\Entity\\InstantPrize', function(InstantPrize $instantPrize) {
                return $instantPrize->getTitle();
            })
            ->addEntityDropdown('prize', 'Supply\\CampaignBundle\\Entity\\Prize', function(Prize $prize) {
                return $prize->getTitle();
            })
            ->add('submitted')
            ->add('submittedAt', 'date', array(
                'input'  => 'datetime',
                'widget' => 'choice',
            ))
            ->add('points');
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'code_form';
    }
}
