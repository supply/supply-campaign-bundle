<?php

namespace Supply\CampaignBundle\Form;

use Kunstmaan\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;

/**
 * The type for Prize
 */
class PrizeAdminType extends AbstractType
{
    /**
     * Builds the form.
     *
     * This method is called for each type in the hierarchy starting form the
     * top most type. Type extensions can further modify the form.
     *
     * @see FormTypeExtensionInterface::buildForm()
     *
     * @param FormBuilderInterface $builder The form builder
     * @param array                $options The options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title');
//        $builder->add('subject');
//        $builder->add('copy');
//        $builder->add('smsSubject');
//        $builder->add('smsCopy');
//        $builder->add('week');
//        $builder->add('weekOrder');
        $builder->add('image',
            MediaType::class,
            array(
                'pattern'  => 'KunstmaanMediaBundle_chooser',
                'required' => false,
            )
        );
//        $builder->add('shieldImage',
//            MediaType::class,
//            array(
//                'pattern'  => 'KunstmaanMediaBundle_chooser',
//                'required' => false,
//            )
//        );
//        $builder->add('emailImage',
//            MediaType::class,
//            array(
//                'pattern'  => 'KunstmaanMediaBundle_chooser',
//                'required' => false,
//            )
//        );
        $builder->add('won');
        $builder->add('expiry', DateTimeType::class, array(
            'input'  => 'datetime',
            'widget' => 'choice',
        ));
        $builder->add('pending', DateTimeType::class, array(
            'input'  => 'datetime',
            'widget' => 'choice',
        ));
        $builder->add('pendingSessionId', 'text');
        $builder->add('activation', DateTimeType::class, array(
            'input'  => 'datetime',
            'widget' => 'choice',
        ));
        $builder->add('type');
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'prize_form';
    }
}
