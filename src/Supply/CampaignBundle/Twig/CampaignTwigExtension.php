<?php
/**
 * Created by PhpStorm.
 * User: hpenny
 * Date: 2/08/16
 * Time: 4:44 PM
 */

namespace Supply\CampaignBundle\Twig;

use Hmp\KumaExtraBundle\Twig\BaseTwigExtension;
use Supply\CampaignBundle\Form\EntryAdminType;
use Supply\CampaignBundle\Form\FrontEnd\EntryType;
use Supply\CampaignBundle\Entity\Entry;

use Supply\CampaignBundle\Helper\Prize\AvailablePrize;
use Supply\CampaignBundle\Services\CampaignPrizeService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class CampaignTwigExtension extends BaseTwigExtension
{
	function getServiceName()
	{
		return 'contentService';
	}

	public function getName()
	{
		return 'supply_campaign_twig_extension';
	}

	public function getFilters()
	{
		return [
		];
	}

	public function prizeService()
    {
	    return $this->container->get('supply.campaign.prize');
    }

	/**
	 * Returns a list of functions to add to the existing list.
	 *
	 * @return array An array of functions
	 */
	public function getFunctions()
	{
		return array(
            'reserve_prize' => new \Twig_Function_Method($this, 'reservePrize'),
            'get_campaign_form' => new \Twig_Function_Method($this, 'getCampaignForm'),
			'translate_rtr_messages' => new \Twig_Function_Method($this, 'translateRtrMessages')
		);
	}

	public function reservePrize(Request $request, $type)
    {

    }

	public function getCampaignForm(Request $request, $formType, $prizeType)
	{

	}

}
