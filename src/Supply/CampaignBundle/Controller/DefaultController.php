<?php

namespace Supply\CampaignBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="supplycampaignbundle_admin_index")
     * @Template()
     */
    public function indexAction()
    {
        return array();
    }
    /**
     * @Route("/dashboard", name="supplycampaignbundle_admin_dashboard")
     * @Template()
     */
    public function dashboardAction()
    {
        return array();
    }
}
