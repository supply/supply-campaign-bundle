<?php
/**
 * Created by PhpStorm.
 * User: henrypenny
 * Date: 6/07/15
 * Time: 11:22 AM
 */
namespace Supply\CampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Campaign
 *
 * @ORM\Table(name="supply_campaign_bundle_campaign")
 * @ORM\Entity(repositoryClass="\Supply\CampaignBundle\Repository\CampaignRepository")
 */
class Campaign extends \Kunstmaan\AdminBundle\Entity\AbstractEntity
{
    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gaCode;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $submitTracking;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Campaign
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set gaCode
     *
     * @param string $gaCode
     *
     * @return Campaign
     */
    public function setGaCode($gaCode)
    {
        $this->gaCode = $gaCode;

        return $this;
    }

    /**
     * Get gaCode
     *
     * @return string
     */
    public function getGaCode()
    {
        return $this->gaCode;
    }

    /**
     * Set submitTracking
     *
     * @param string $submitTracking
     *
     * @return Campaign
     */
    public function setSubmitTracking($submitTracking)
    {
        $this->submitTracking = $submitTracking;

        return $this;
    }

    /**
     * Get submitTracking
     *
     * @return string
     */
    public function getSubmitTracking()
    {
        return $this->submitTracking;
    }
}
