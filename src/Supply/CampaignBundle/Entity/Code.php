<?php

namespace Supply\CampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Faker\Provider\cs_CZ\DateTime;

/**
 * Code
 *
 * @ORM\Table(name="supply_campaign_bundle_code")
 * @ORM\Entity(repositoryClass="Supply\CampaignBundle\Repository\CodeRepository")
 */
class Code extends \Kunstmaan\AdminBundle\Entity\AbstractEntity
{
    /**
     * @var Campaign
     *
     * @ORM\ManyToOne(targetEntity="Supply\CampaignBundle\Entity\Campaign")
     * @ORM\JoinColumn(name="campaign_id", referencedColumnName="id")
     */
    protected $campaign;

    /**
     * @var InstantPrize
     *
     * @ORM\ManyToOne(targetEntity="Supply\CampaignBundle\Entity\InstantPrize")
     * @ORM\JoinColumn(name="instant_prize_id", referencedColumnName="id")
     */
    protected $instantPrize;

    /**
     * @var Prize
     *
     * @ORM\ManyToOne(targetEntity="Supply\CampaignBundle\Entity\Prize")
     * @ORM\JoinColumn(name="prize_id", referencedColumnName="id")
     */
    protected $prize;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255)
     */
    private $code;

    /**
     * @var boolean
     *
     * @ORM\Column(name="submitted", type="boolean")
     */
    private $submitted = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="submitted_at", type="datetime", nullable=true  )
     */
    private $submittedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="generated_timestamp", type="integer")
     */
    private $generatedTimestamp;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer")
     */
    private $points;

    /**
     * @param \DateTime|null $ts
     * @return $this
     */
    public function doSubmit(\DateTime $ts = null)
    {
        if($this->getSubmitted()) {
            throw new \Exception('The code ' . $this->code . ' has already been submitted');
        }
        $this->setSubmitted(true);
        $this->setSubmittedAt($ts?$ts:new \DateTime());
        return $this;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Code
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set submitted
     *
     * @param boolean $submitted
     *
     * @return Code
     */
    public function setSubmitted($submitted)
    {
        $this->submitted = $submitted;

        return $this;
    }

    /**
     * Get submitted
     *
     * @return boolean
     */
    public function getSubmitted()
    {
        return $this->submitted;
    }

    /**
     * Set submittedAt
     *
     * @param \DateTime $submittedAt
     *
     * @return Code
     */
    public function setSubmittedAt($submittedAt)
    {
        $this->submittedAt = $submittedAt;

        return $this;
    }

    /**
     * Get submittedAt
     *
     * @return \DateTime
     */
    public function getSubmittedAt()
    {
        return $this->submittedAt;
    }

    /**
     * Set campaign
     *
     * @param \Supply\CampaignBundle\Entity\Campaign $campaign
     *
     * @return Code
     */
    public function setCampaign(\Supply\CampaignBundle\Entity\Campaign $campaign = null)
    {
        $this->campaign = $campaign;

        return $this;
    }

    /**
     * Get campaign
     *
     * @return \Supply\CampaignBundle\Entity\Campaign
     */
    public function getCampaign()
    {
        return $this->campaign;
    }

    /**
     * Set instantPrize
     *
     * @param \Supply\CampaignBundle\Entity\InstantPrize $instantPrize
     *
     * @return Code
     */
    public function setInstantPrize(\Supply\CampaignBundle\Entity\InstantPrize $instantPrize = null)
    {
        $this->instantPrize = $instantPrize;

        return $this;
    }

    /**
     * Get instantPrize
     *
     * @return \Supply\CampaignBundle\Entity\InstantPrize
     */
    public function getInstantPrize()
    {
        return $this->instantPrize;
    }

    /**
     * Set prize
     *
     * @param \Supply\CampaignBundle\Entity\Prize $prize
     *
     * @return Code
     */
    public function setPrize(\Supply\CampaignBundle\Entity\Prize $prize = null)
    {
        $this->prize = $prize;

        return $this;
    }

    /**
     * Get prize
     *
     * @return \Supply\CampaignBundle\Entity\Prize
     */
    public function getPrize()
    {
        return $this->prize;
    }

    /**
     * Set generatedTimestamp
     *
     * @param $generatedTimestamp
     *
     * @return Code
     */
    public function setGeneratedTimestamp($generatedTimestamp)
    {
        $this->generatedTimestamp = $generatedTimestamp;

        return $this;
    }

    /**
     * Get generatedTimestamp
     *
     * @return \int
     */
    public function getGeneratedTimestamp()
    {
        return $this->generatedTimestamp;
    }

    /**
     * Set points
     *
     * @param integer $points
     *
     * @return Code
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }
}
