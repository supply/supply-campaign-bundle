<?php

namespace Supply\CampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Kunstmaan\NodeBundle\Entity\NodeTranslation;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entry
 *
 * @ORM\Table(name="supply_campaign_bundle_entry")
 * @ORM\Entity(repositoryClass="\Supply\CampaignBundle\Repository\EntryRepository")
 */
class Entry extends \Kunstmaan\AdminBundle\Entity\AbstractEntity
{
    const SMS = 'sms';
    const WEB = 'web';

    /**
     * @var string
     * @ORM\Column(name="receipt_entry_code", type="string", length=255, nullable=true)
     */
    private $receiptEntryCode;

    /**
     * @var Code
     *
     * @ORM\OneToOne(targetEntity="Supply\CampaignBundle\Entity\Code")
     * @ORM\JoinColumn(name="code_id", referencedColumnName="id")
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @Assert\Email(message = "Please enter a valid email address", groups={"frontEnd"})
     * @ORM\Column(name="email_address", type="string", length=255, nullable=true)
     */
    private $emailAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile_number", type="string", length=255, nullable=true)
     */
    private $mobileNumber;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="address", type="string", length=255, nullable=true)
	 */
	private $address;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="suburb", type="string", length=255, nullable=true)
	 */
	private $suburb;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="post_code", type="string", length=255, nullable=true)
	 */
	private $postCode;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255, nullable=true)
     */
    private $companyName;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255, nullable=true)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="work_address", type="string", length=255, nullable=true)
     */
    private $workAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="industry", type="string", length=255, nullable=true)
     */
    private $industry;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="entry_date", type="datetime")
     */
    private $entryDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reminder_date", type="datetime", nullable=true)
     */
    private $reminderDate;

    /**
     * @var string
     *
     * @ORM\Column(name="channel", type="string", length=255, nullable=true)
     */
    private $channel;

    /**
     * @var Prize
     *
     * @ORM\ManyToOne(targetEntity="Supply\CampaignBundle\Entity\Prize")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prize_id", referencedColumnName="id")
     * })
     */
    private $prize;

    /**
     * @var $reminderSent
     *
     * @ORM\Column(name="reminder_sent", type="boolean", nullable=true)
     */
    private $reminderSent = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="age_verify", type="boolean", nullable=true)
     */
    private $ageVerify;

    /**
     * @var \DateTime $dob;
     *
     * @ORM\Column(name="dob", type="date", nullable=true)
     */
    private $dob;

    /**
     * @var boolean
     *
     * @ORM\Column(name="terms", type="boolean", nullable=true)
     */
    private $terms;

    /**
     * @var boolean
     *
     * @ORM\Column(name="opt_in", type="boolean", nullable=true)
     */
    private $optIn;

    /**
     * @var string
     *
     * @ORM\Column(name="spare_1", type="string", length=255, nullable=true)
     */
    private $spare1;

    /**
     * @var string
     *
     * @ORM\Column(name="spare_2", type="string", length=255, nullable=true)
     */
    private $spare2;

    /**
     * @var string
     *
     * @ORM\Column(name="spare_3", type="string", length=255, nullable=true)
     */
    private $spare3;

    /**
     * @var string
     *
     * @ORM\Column(name="spare_4", type="string", length=255, nullable=true)
     */
    private $spare4;

    /**
     * @var string
     *
     * @ORM\Column(name="spare_5", type="string", length=255, nullable=true)
     */
    private $spare5;


    /**
     * @var string
     *
     * @ORM\Column(name="session_id", type="text", nullable=true)
     */
    private $sessionId;

    public function __construct()
    {
        $this->entryDate = new \Datetime();
    }

    /**
     * Set receiptEntryCode
     *
     * @param string $receiptEntryCode
     *
     * @return Entry
     */
    public function setReceiptEntryCode($receiptEntryCode)
    {
        $this->receiptEntryCode = $receiptEntryCode;

        return $this;
    }

    /**
     * Get receiptEntryCode
     *
     * @return string
     */
    public function getReceiptEntryCode()
    {
        return $this->receiptEntryCode;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Entry
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Entry
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Entry
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     *
     * @return Entry
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set vibSignup
     *
     * @param boolean $vibSignup
     *
     * @return Entry
     */
    public function setVibSignup($vibSignup)
    {
        $this->vibSignup = $vibSignup;

        return $this;
    }

    /**
     * Get vibSignup
     *
     * @return boolean
     */
    public function getVibSignup()
    {
        return $this->vibSignup;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Entry
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set entryDate
     *
     * @param \DateTime $entryDate
     *
     * @return Entry
     */
    public function setEntryDate($entryDate)
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    /**
     * Get entryDate
     *
     * @return \DateTime
     */
    public function getEntryDate()
    {
        return $this->entryDate;
    }

    /**
     * Get entryDate
     *
     * @return \DateTime
     */
    public function getExpiryDate()
    {
        $expiryDate = clone $this->entryDate;
        $expiryDate->add(new \DateInterval('P14D'));
        return $expiryDate->format('d-m-Y');
    }

    /**
     * Set channel
     *
     * @param string $channel
     *
     * @return Entry
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set prize
     *
     * @param Prize $prize
     *
     * @return Entry
     */
    public function setPrize(Prize $prize = null)
    {
        $this->prize = $prize;

        return $this;
    }

    /**
     * Get prize
     *
     * @return Prize
     */
    public function getPrize()
    {
        return $this->prize;
    }

    /**
     * Set reminderDate
     *
     * @param \DateTime $reminderDate
     *
     * @return Entry
     */
    public function setReminderDate($reminderDate)
    {
        $this->reminderDate = $reminderDate;

        return $this;
    }

    /**
     * Get reminderDate
     *
     * @return \DateTime
     */
    public function getReminderDate()
    {
        return $this->reminderDate;
    }

    /**
     * Set reminderSent
     *
     * @param boolean $reminderSent
     *
     * @return Entry
     */
    public function setReminderSent($reminderSent)
    {
        $this->reminderSent = $reminderSent;

        return $this;
    }

    /**
     * Get reminderSent
     *
     * @return boolean
     */
    public function getReminderSent()
    {
        return $this->reminderSent;
    }

	/**
	 * Set address
	 *
	 * @param string $address
	 *
	 * @return Entry
	 */
	public function setAddress($address)
	{
		$this->address = $address;

		return $this;
	}

	/**
	 * Get address
	 *
	 * @return string
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * Set suburb
	 *
	 * @param string $suburb
	 *
	 * @return Entry
	 */
	public function setSuburb($suburb)
	{
		$this->suburb = $suburb;

		return $this;
	}

	/**
	 * Get suburb
	 *
	 * @return string
	 */
	public function getSuburb()
	{
		return $this->suburb;
	}

	/**
	 * Set postCode
	 *
	 * @param string $postCode
	 *
	 * @return Entry
	 */
	public function setPostCode($postCode)
	{
		$this->postCode = $postCode;

		return $this;
	}

	/**
	 * Get postCode
	 *
	 * @return string
	 */
	public function getPostCode()
	{
		return $this->postCode;
	}

    /**
     * Set sessionId
     *
     * @param string $sessionId
     *
     * @return Entry
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;

        return $this;
    }

    /**
     * Get sessionId
     *
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }


    /**
     * Set ageVerify
     *
     * @param string $ageVerify
     *
     * @return Prize
     */
    public function setAgeVerify($ageVerify)
    {
        $this->ageVerify = $ageVerify;

        return $this;
    }

    /**
     * Get type
     *
     * @return ageVerify
     */
    public function getAgeVerify()
    {
        return $this->ageVerify;
    }

    /**
     * Set terms
     *
     * @param string $terms
     *
     * @return Prize
     */
    public function setTerms($terms)
    {
        $this->terms = $terms;

        return $this;
    }

    /**
     * Get terms
     *
     * @return string
     */
    public function getTerms()
    {
        return $this->terms;
    }


    /**
     * Set optIn
     *
     * @param boolean $optIn
     *
     * @return Entry
     */
    public function setOptIn($optIn)
    {
        $this->optIn = $optIn;

        return $this;
    }

    /**
     * Get optIn
     *
     * @return boolean
     */
    public function getOptIn()
    {
        return $this->optIn;
    }

    /**
     * Set mobileNumber
     *
     * @param string $mobileNumber
     *
     * @return Entry
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * Get mobileNumber
     *
     * @return string
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return Entry
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return Entry
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set workAddress
     *
     * @param string $workAddress
     *
     * @return Entry
     */
    public function setWorkAddress($workAddress)
    {
        $this->workAddress = $workAddress;

        return $this;
    }

    /**
     * Get workAddress
     *
     * @return string
     */
    public function getWorkAddress()
    {
        return $this->workAddress;
    }

    /**
     * Set industry
     *
     * @param string $industry
     *
     * @return Entry
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;

        return $this;
    }

    /**
     * Get industry
     *
     * @return string
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * Set spare1
     *
     * @param string $spare1
     *
     * @return Entry
     */
    public function setSpare1($spare1)
    {
        $this->spare1 = $spare1;

        return $this;
    }

    /**
     * Get spare1
     *
     * @return string
     */
    public function getSpare1()
    {
        return $this->spare1;
    }

    /**
     * Set spare2
     *
     * @param string $spare2
     *
     * @return Entry
     */
    public function setSpare2($spare2)
    {
        $this->spare2 = $spare2;

        return $this;
    }

    /**
     * Get spare2
     *
     * @return string
     */
    public function getSpare2()
    {
        return $this->spare2;
    }

    /**
     * Set spare3
     *
     * @param string $spare3
     *
     * @return Entry
     */
    public function setSpare3($spare3)
    {
        $this->spare3 = $spare3;

        return $this;
    }

    /**
     * Get spare3
     *
     * @return string
     */
    public function getSpare3()
    {
        return $this->spare3;
    }

    /**
     * Set spare4
     *
     * @param string $spare4
     *
     * @return Entry
     */
    public function setSpare4($spare4)
    {
        $this->spare4 = $spare4;

        return $this;
    }

    /**
     * Get spare4
     *
     * @return string
     */
    public function getSpare4()
    {
        return $this->spare4;
    }

    /**
     * Set spare5
     *
     * @param string $spare5
     *
     * @return Entry
     */
    public function setSpare5($spare5)
    {
        $this->spare5 = $spare5;

        return $this;
    }

    /**
     * Get spare5
     *
     * @return string
     */
    public function getSpare5()
    {
        return $this->spare5;
    }

    /**
     * Set code
     *
     * @param \Supply\CampaignBundle\Entity\Code $code
     *
     * @return Entry
     */
    public function setCode(\Supply\CampaignBundle\Entity\Code $code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return \Supply\CampaignBundle\Entity\Code
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set dob
     *
     * @param \DateTime $dob
     *
     * @return Entry
     */
    public function setDob($dob)
    {
        $this->dob = $dob;

        return $this;
    }

    /**
     * Get dob
     *
     * @return \DateTime
     */
    public function getDob()
    {
        return $this->dob;
    }
}
