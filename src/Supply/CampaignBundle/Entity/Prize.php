<?php

namespace Supply\CampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Prize
 *
 * @ORM\Table(name="supply_campaign_bundle_prize")
 * @ORM\Entity(repositoryClass="\Supply\CampaignBundle\Repository\PrizeRepository")
 */
class Prize extends \Kunstmaan\AdminBundle\Entity\AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text", nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="copy", type="text", nullable=true)
     */
    private $copy;

    /**
     * @var string
     *
     * @ORM\Column(name="sms_subject", type="text", nullable=true)
     */
    private $smsSubject;

    /**
     * @var string
     *
     * @ORM\Column(name="sms_copy", type="text", nullable=true)
     */
    private $smsCopy;

    /**
     * @var string
     *
     * @ORM\Column(name="week", type="text", nullable=true)
     */
    private $week;

    /**
     * @var integer
     *
     * @ORM\Column(name="week_order", type="smallint", nullable=true)
     */
    private $weekOrder;

    /**
     * @var \Kunstmaan\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     * })
     */
    private $image;

    /**
     * @var \Kunstmaan\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shield_image_id", referencedColumnName="id")
     * })
     */
    private $shieldImage;

    /**
     * @var \Kunstmaan\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="email_image_id", referencedColumnName="id")
     * })
     */
    private $emailImage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="won", type="boolean", nullable=true)
     */
    private $won;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expiry", type="datetime", nullable=true)
     */
    private $expiry;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="activation", type="datetime", nullable=true)
     */
    private $activation;

    /**
     * @var boolean
     *
     * @ORM\Column(name="pending", type="datetime", nullable=true)
     */
    private $pending;

    /**
     * @var string
     *
     * @ORM\Column(name="pending_session_id", type="text", nullable=true)
     */
    private $pendingSessionId;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Supply\CampaignBundle\Entity\Entry", mappedBy="prize", fetch="EXTRA_LAZY")
     */
    private $entrants;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Prize
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set copy
     *
     * @param string $copy
     *
     * @return Prize
     */
    public function setCopy($copy)
    {
        $this->copy = $copy;

        return $this;
    }

    /**
     * Get copy
     *
     * @return string
     */
    public function getCopy()
    {
        return $this->copy;
    }

    /**
     * Set week
     *
     * @param string $week
     *
     * @return Prize
     */
    public function setWeek($week)
    {
        $this->week = $week;

        return $this;
    }

    /**
     * Get week
     *
     * @return string
     */
    public function getWeek()
    {
        return $this->week;
    }

    /**
     * Set weekOrder
     *
     * @param integer $weekOrder
     *
     * @return Prize
     */
    public function setWeekOrder($weekOrder)
    {
        $this->weekOrder = $weekOrder;

        return $this;
    }

    /**
     * Get weekOrder
     *
     * @return integer
     */
    public function getWeekOrder()
    {
        return $this->weekOrder;
    }

    /**
     * Set image
     *
     * @param \Kunstmaan\MediaBundle\Entity\Media $image
     * @return Prize
     */
    public function setImage(\Kunstmaan\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Kunstmaan\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set shieldImage
     *
     * @param \Kunstmaan\MediaBundle\Entity\Media $shieldImage
     * @return Prize
     */
    public function setShieldImage(\Kunstmaan\MediaBundle\Entity\Media $shieldImage = null)
    {
        $this->shieldImage = $shieldImage;

        return $this;
    }

    /**
     * Get shieldImage
     *
     * @return \Kunstmaan\MediaBundle\Entity\Media
     */
    public function getShieldImage()
    {
        return $this->shieldImage;
    }

    /**
     * Set won
     *
     * @param boolean $won
     *
     * @return Prize
     */
    public function setWon($won)
    {
        $this->won = $won;

        return $this;
    }

    /**
     * Get won
     *
     * @return boolean
     */
    public function getWon()
    {
        return $this->won;
    }

    /**
     * Set emailImage
     *
     * @param \Kunstmaan\MediaBundle\Entity\Media $emailImage
     *
     * @return Prize
     */
    public function setEmailImage(\Kunstmaan\MediaBundle\Entity\Media $emailImage = null)
    {
        $this->emailImage = $emailImage;

        return $this;
    }

    /**
     * Get emailImage
     *
     * @return \Kunstmaan\MediaBundle\Entity\Media
     */
    public function getEmailImage()
    {
        return $this->emailImage;
    }

    /**
     * Set expiry
     *
     * @param \DateTime $expiry
     *
     * @return Prize
     */
    public function setExpiry($expiry)
    {
        $this->expiry = $expiry;

        return $this;
    }

    /**
     * Get expiry
     *
     * @return \DateTime
     */
    public function getExpiry()
    {
        return $this->expiry;
    }


    /**
     * Get expiry
     *
     * @return \DateTime
     */
    public function getExpiryDate()
    {
        if($this->expiry) {
            return $this->expiry->format('Y-m-d');
        }
        else {
            return '';
        }
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entrants = new \Doctrine\Common\Collections\ArrayCollection();
        $this->expiry = new \DateTime('+ 1 year');
        $this->activation = new \DateTime();
    }

    /**
     * Add entrant
     *
     * @param Entry $entrant
     *
     * @return Prize
     */
    public function addEntrant(Entry $entrant)
    {
        $this->entrants[] = $entrant;

        return $this;
    }

    /**
     * Remove entrant
     *
     * @param Entry $entrant
     */
    public function removeEntrant(Entry $entrant)
    {
        $this->entrants->removeElement($entrant);
    }

    /**
     * Get entrants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntrants()
    {
        return $this->entrants;
    }

    /**
     * Set smsCopy
     *
     * @param string $smsCopy
     *
     * @return Prize
     */
    public function setSmsCopy($smsCopy)
    {
        $this->smsCopy = $smsCopy;

        return $this;
    }

    /**
     * Get smsCopy
     *
     * @return string
     */
    public function getSmsCopy()
    {
        return $this->smsCopy;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return Prize
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set smsSubject
     *
     * @param string $smsSubject
     *
     * @return Prize
     */
    public function setSmsSubject($smsSubject)
    {
        $this->smsSubject = $smsSubject;

        return $this;
    }

    /**
     * Get smsSubject
     *
     * @return string
     */
    public function getSmsSubject()
    {
        return $this->smsSubject;
    }

    /**
     * Set activation
     *
     * @param \DateTime $activation
     *
     * @return Prize
     */
    public function setActivation($activation)
    {
        $this->activation = $activation;

        return $this;
    }

    /**
     * Get activation
     *
     * @return \DateTime
     */
    public function getActivation()
    {
        return $this->activation;
    }

    /**
     * Set pending
     *
     * @param datetime $pending
     *
     * @return Prize
     */
    public function setPending($pending)
    {
        $this->pending = $pending;

        return $this;
    }

    /**
     * Get pending
     *
     * @return \DateTime
     */
    public function getPending()
    {
        return $this->pending;
    }

    /**
     * Set pendingSessionId
     *
     * @param datetime $pendingSessionId
     *
     * @return Prize
     */
    public function setPendingSessionId($pendingSessionId)
    {
        $this->pendingSessionId = $pendingSessionId;

        return $this;
    }

    /**
     * Get pendingSessionId
     *
     * @return \DateTime
     */
    public function getPendingSessionId()
    {
        return $this->pendingSessionId;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Prize
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
