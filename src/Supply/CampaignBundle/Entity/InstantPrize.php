<?php

namespace Supply\CampaignBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InstantPrize
 *
 * @ORM\Table(name="supply_campaign_bundle_instant_prize")
 * @ORM\Entity(repositoryClass="\Supply\CampaignBundle\Repository\InstantPrizeRepository")
 */
class InstantPrize extends \Kunstmaan\AdminBundle\Entity\AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text", nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="copy", type="text", nullable=true)
     */
    private $copy;

    /**
     * @var string
     *
     * @ORM\Column(name="sms_subject", type="text", nullable=true)
     */
    private $smsSubject;

    /**
     * @var string
     *
     * @ORM\Column(name="sms_copy", type="text", nullable=true)
     */
    private $smsCopy;

    /**
     * @var \Kunstmaan\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="Kunstmaan\MediaBundle\Entity\Media")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     * })
     */
    private $image;

    /**
     * @var int
     *
     * @ORM\Column(name="weight", type="smallint", nullable=true)
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="ts_and_cs", type="text", nullable=true)
     */
    private $tsAndCs;

    /**
     * Set title
     *
     * @param string $title
     *
     * @return InstantPrize
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set copy
     *
     * @param string $copy
     *
     * @return InstantPrize
     */
    public function setCopy($copy)
    {
        $this->copy = $copy;

        return $this;
    }

    /**
     * Get copy
     *
     * @return string
     */
    public function getCopy()
    {
        return $this->copy;
    }

    /**
     * Set image
     *
     * @param \Kunstmaan\MediaBundle\Entity\Media $image
     * @return InstantPrize
     */
    public function setImage(\Kunstmaan\MediaBundle\Entity\Media $image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return \Kunstmaan\MediaBundle\Entity\Media
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set weight
     *
     * @param integer $weight
     *
     * @return InstantPrize
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return integer
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set smsCopy
     *
     * @param string $smsCopy
     *
     * @return InstantPrize
     */
    public function setSmsCopy($smsCopy)
    {
        $this->smsCopy = $smsCopy;

        return $this;
    }

    /**
     * Get smsCopy
     *
     * @return string
     */
    public function getSmsCopy()
    {
        return $this->smsCopy;
    }

    /**
     * Set tsAndCs
     *
     * @param string $tsAndCs
     *
     * @return InstantPrize
     */
    public function setTsAndCs($tsAndCs)
    {
        $this->tsAndCs = $tsAndCs;

        return $this;
    }

    /**
     * Get tsAndCs
     *
     * @return string
     */
    public function getTsAndCs()
    {
        return $this->tsAndCs;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return InstantPrize
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set smsSubject
     *
     * @param string $smsSubject
     *
     * @return InstantPrize
     */
    public function setSmsSubject($smsSubject)
    {
        $this->smsSubject = $smsSubject;

        return $this;
    }

    /**
     * Get smsSubject
     *
     * @return string
     */
    public function getSmsSubject()
    {
        return $this->smsSubject;
    }
}
