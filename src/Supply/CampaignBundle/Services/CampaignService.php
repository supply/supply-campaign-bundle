<?php
/**
 * Created by PhpStorm.
 * User: henrypenny
 * Date: 24/06/15
 * Time: 10:33 AM
 */

namespace Supply\CampaignBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Guzzle\Http\Client as GuzzleClient;
use Kunstmaan\NodeBundle\Entity\Node;
use Kunstmaan\NodeBundle\Repository\NodeRepository;
use Kunstmaan\PagePartBundle\Entity\PagePartRef;
use Kunstmaan\PagePartBundle\Repository\PagePartRefRepository;
use Supply\CampaignBundle\Entity\Campaign;
use Symfony\Bundle\TwigBundle\TwigEngine;

class CampaignService
{
    /**
     * @var EntityManager $em
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getCampaign()
    {
        $campaign = $this->em->getRepository('BurgerfuelCmsBundle:Campaign')->find(1);
        if(!$campaign) {
            $campaign = new Campaign();
            $campaign->setName('New campaign')
                ->setGaCode('ERROR - check admin/campaigns')
                ->setSubmitTracking("alert('ERROR - check admin/campaigns')");
            $this->em->persist($campaign);
            $this->em->flush($campaign);
        }
        return $campaign;
    }
}
