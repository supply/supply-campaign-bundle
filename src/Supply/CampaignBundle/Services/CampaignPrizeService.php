<?php
/**
 * Created by PhpStorm.
 * User: henrypenny
 * Date: 24/06/15
 * Time: 10:33 AM
 */

namespace Supply\CampaignBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\QueryBuilder;
use Guzzle\Http\Client as GuzzleClient;
use Kunstmaan\NodeBundle\Entity\Node;
use Kunstmaan\NodeBundle\Repository\NodeRepository;
use Kunstmaan\PagePartBundle\Entity\PagePartRef;
use Kunstmaan\PagePartBundle\Repository\PagePartRefRepository;
use Supply\CampaignBundle\Entity\Entry;
use Supply\CampaignBundle\Entity\InstantPrize;
use Supply\CampaignBundle\Entity\Prize;
use Supply\CampaignBundle\Helper\Prize\AvailablePrize;
use Supply\CampaignBundle\Repository\PrizeRepository;
use Symfony\Bundle\TwigBundle\TwigEngine;

class CampaignPrizeService
{
    /**
     * @var EntityManager $em
     */
    protected $em;

    /**
     * @var TwigEngine $templating
     */
    protected $templating;

    /**
     * @var array
     */
    protected $config;

    /**
     * @param $type
     * @return string
     */
    public function getFormType($type)
    {
        switch($type) {
            case "instant":
                return "App\\Bundle\\Form\\Entry\\WinnerType";
            default:
                return $type;
        }
    }

    public function __construct(EntityManager $entityManager, TwigEngine $templating, $config)
    {
        $this->em = $entityManager;
        $this->templating = $templating;
        $this->config = $config;
    }

    /**
     * @return null|Prize
     */
    public function getMajorPrize()
    {
        /** @var NodeRepository $nodeRepo */
        $nodeRepo = $this->em->getRepository('KunstmaanNodeBundle:Node');

        /** @var PagePartRefRepository $ppRepo */
        $ppRepo = $this->em->getRepository('KunstmaanPagePartBundle:PagePartRef');

        /** @var Node[] $nodes */
        $nodes = $nodeRepo->findBy(
            [
                'parent' => $nodeRepo->find(1),
            ]
        );

        foreach($nodes as $node) {
            if($node->getRefEntityName() == 'Supply\CampaignBundle\Entity\Pages\USAyePage') {
                /** @var USAyePage $page */
                $page = $node->getNodeTranslation('en')->getPublicNodeVersion()->getRef($this->em);
                $pageparts = $ppRepo->getPageParts($page, 'home_page_prize');
                if(count($pageparts)) {
                    $pagepart = $pageparts[0];

                    if($pagepart instanceof PrizePagePart) {
                        /** @var PrizePagePart $pagepart */
                        return $pagepart->getPrize();
                    }
                }
            }
        }

        error_log('Failed to find Major Prize from homepage page part - defaulting to Prize.id = 1');

        $prize = $this->em->getRepository('SupplyCampaignBundle:Prize')->find(1);
        return $prize;
    }

    /**
     * @return InstantPrize
     */
    public function getRandomPrize()
    {
        $prizes = $this->em->getRepository('SupplyCampaignBundle:InstantPrize')->findAll();

        $prizeLookup = [];
        $totalWeight = 0 ;

        foreach($prizes as $prize) {
            $totalWeight += $prize->getWeight();
            for ($i = 0; $i < $prize->getWeight(); $i++) {
                $prizeLookup[] = $prize;
            }
        }

        $index = rand(0, $totalWeight - 1);
        $prize = $prizeLookup[$index];
        return $prize;
    }

    public function mergePrizeCopy($copy, Prize $prize, InstantPrize $instantPrize, Entry $entry)
    {
        $result = $this->templating->render('BurgerfuelCmsBundle:USAye/Emails:merge.html.twig',
            compact('copy', 'prize', 'instantPrize', 'entry')
        );
        return $result;
    }

    public function getPrizeQueryBuilder($type, $activationTime = null)
    {
        $activationTime = $activationTime?:new \DateTime();

        /** @var PrizeRepository $prizeRepo */
        $prizeRepo = $this->em->getRepository('SupplyCampaignBundle:Prize');

        /** @var QueryBuilder $qb */
        $qb = $prizeRepo->createQueryBuilder('p')
            ->where('p.type = :type')
            ->andWhere('p.activation <= :now')
            ->setParameter('type', $type)
            ->setParameter('now', $activationTime)
            ->orderBy('p.activation')
            ->setFirstResult(0)
            ->setMaxResults(1)
        ;

        return $qb;
    }

    /**
     * @param $type string
     * @param $sessionId string
     * @return AvailablePrize|Hmp\KumaExtraBundle\Form\FormBuilder|Supply\CampaignBundle\Controller\PrizeAdminListController|Symfony\Component\HttpFoundation\Request
     */
    public function getAvailablePrize($type, $sessionId)
    {
        if(!$sessionId) {
            throw new \InvalidArgumentException('SessionId must be available');
        }
        $qb = $this->getPrizeQueryBuilder($type);
        $qb->andWhere('p.won = 0');
        $qb->andWhere('p.pendingSessionId is null or p.pendingSessionId = :pendingSessionId OR p.pending is null or p.pending <= :now');
        $qb->setParameter('pendingSessionId', $sessionId);

        $query = $qb->getQuery();
        $results = $query->getResult();

        if(count($results)) {
            $em = $this->em;
            /** @var Prize $prize */
            $prize = $results[0];
            $ap = new AvailablePrize($prize, function(\DateTime $pending, $sessionId) use ($prize, $em) {

                if(!$pending) {
                    throw new \Exception('pending cannot be null');
                }
                if(!$sessionId) {
                    throw new \Exception('sessionId cannot be null');
                }

                $prize
                    ->setPending($pending)
                    ->setPendingSessionId($sessionId);
                $em->persist($prize);
                $em->flush($prize);
                return $prize;
            }, $sessionId);
            return $ap;
        }
        else {
            return new AvailablePrize(null, null, null);
        }
    }

    /**
     * @param $type string
     * @param $sessionId string
     * @return \Supply\CampaignBundle\Entity\Prize|null
     */
    public function getReservedPrize($type, $sessionId)
    {
        $qb = $this->getPrizeQueryBuilder($type, new \DateTime());
        $qb->andWhere('p.pendingSessionId = :pendingSessionId')
            ->setParameter('pendingSessionId', $sessionId);
        $query = $qb->getQuery();
        /** @var Prize[] $results */
        $results = $query->getResult();

        if(count($results)) {
            return $results[0];
        }
        else {
            return null;
        }
    }

    /**
     * @param $entryId string
     * @param $sessionId string
     * @return null|Entry
     */
    public function getPrizeByEntryIdSessionId($entryId, $sessionId)
    {
        /** @var Entry $entry */
        $entry = $this->em->find('SupplyCampaignBundle:Entry', $entryId);
        if($entry && $entry->getSessionId() && $entry->getSessionId() == $sessionId) {
            return $entry->getPrize();
        }
        else {
            return null;
        }
    }
}
