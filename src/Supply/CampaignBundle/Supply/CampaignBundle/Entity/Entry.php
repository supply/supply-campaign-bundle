<?php

namespace Supply\CampaignBundle\Entity;

/**
 * Entry
 */
class Entry
{
    /**
     * @var string
     */
    private $receiptEntryCode;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $emailAddress;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $suburb;

    /**
     * @var string
     */
    private $postCode;

    /**
     * @var \DateTime
     */
    private $entryDate;

    /**
     * @var \DateTime
     */
    private $reminderDate;

    /**
     * @var string
     */
    private $channel;

    /**
     * @var boolean
     */
    private $reminderSent;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Supply\CampaignBundle\Entity\Prize
     */
    private $prize;


    /**
     * Set receiptEntryCode
     *
     * @param string $receiptEntryCode
     *
     * @return Entry
     */
    public function setReceiptEntryCode($receiptEntryCode)
    {
        $this->receiptEntryCode = $receiptEntryCode;

        return $this;
    }

    /**
     * Get receiptEntryCode
     *
     * @return string
     */
    public function getReceiptEntryCode()
    {
        return $this->receiptEntryCode;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Entry
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Entry
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Entry
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     *
     * @return Entry
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return Entry
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Entry
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set suburb
     *
     * @param string $suburb
     *
     * @return Entry
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set postCode
     *
     * @param string $postCode
     *
     * @return Entry
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;

        return $this;
    }

    /**
     * Get postCode
     *
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * Set entryDate
     *
     * @param \DateTime $entryDate
     *
     * @return Entry
     */
    public function setEntryDate($entryDate)
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    /**
     * Get entryDate
     *
     * @return \DateTime
     */
    public function getEntryDate()
    {
        return $this->entryDate;
    }

    /**
     * Set reminderDate
     *
     * @param \DateTime $reminderDate
     *
     * @return Entry
     */
    public function setReminderDate($reminderDate)
    {
        $this->reminderDate = $reminderDate;

        return $this;
    }

    /**
     * Get reminderDate
     *
     * @return \DateTime
     */
    public function getReminderDate()
    {
        return $this->reminderDate;
    }

    /**
     * Set channel
     *
     * @param string $channel
     *
     * @return Entry
     */
    public function setChannel($channel)
    {
        $this->channel = $channel;

        return $this;
    }

    /**
     * Get channel
     *
     * @return string
     */
    public function getChannel()
    {
        return $this->channel;
    }

    /**
     * Set reminderSent
     *
     * @param boolean $reminderSent
     *
     * @return Entry
     */
    public function setReminderSent($reminderSent)
    {
        $this->reminderSent = $reminderSent;

        return $this;
    }

    /**
     * Get reminderSent
     *
     * @return boolean
     */
    public function getReminderSent()
    {
        return $this->reminderSent;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prize
     *
     * @param \Supply\CampaignBundle\Entity\Prize $prize
     *
     * @return Entry
     */
    public function setPrize(\Supply\CampaignBundle\Entity\Prize $prize = null)
    {
        $this->prize = $prize;

        return $this;
    }

    /**
     * Get prize
     *
     * @return \Supply\CampaignBundle\Entity\Prize
     */
    public function getPrize()
    {
        return $this->prize;
    }
}
