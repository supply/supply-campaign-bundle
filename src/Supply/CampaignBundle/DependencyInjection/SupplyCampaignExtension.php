<?php

namespace Supply\CampaignBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SupplyCampaignExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
	    $loader->load('services.yml');
        $loader->load('twig.yml');
        $loader->load('menus.yml');

        $this->addConfigToContainer($container, $config, $this->getAlias());
    }

    public function addConfigToContainer($container, $config, $ns)
    {
        if(is_array($config)) {
            foreach($config as $index => $child) {
                $this->addConfigToContainer($container, $child, $ns . '_' . $index);
            }
        }
        else {
            $container->setParameter($ns, $config);
        }
    }

}
